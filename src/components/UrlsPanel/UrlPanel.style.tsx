import styled from "styled-components";
import {device} from '../../styles/mediaQuery';

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 4rem 10%;
  ${device.md`
    margin: 2rem 0;
  `};
`;

export const Group = styled.div`
  display: flex;
  align-items: center;
  min-height: 4rem;
  background-color: #f8f8f8;
  width: 100%;
  margin-top: 0.4rem;
  ${device.md`
    flex-direction: column;
  `};
`;

export const Column = styled.div`
  padding-left: 2rem;
  flex-basis: 50%;
  text-align: left;
  font-size: 1.4rem;
  color: #06053c;
  line-height: 4rem;
  ${device.md`
    flex-basis: 100%;
    padding-left: 0;
  `};
`;

export const TheLink = styled.a`
  text-decoration: none;
  color: #00c3b9;
`;