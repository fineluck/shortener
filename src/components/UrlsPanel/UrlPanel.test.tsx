import React from 'react';
import {render} from '@testing-library/react';
import {UrlsPanel} from './UrlsPanel';
import type {UrlType} from '../../type/type';

const setup = (mockData: UrlType[]) => {
  const utils = render(<UrlsPanel urlData={mockData}/>);
  const panel = utils.getAllByLabelText('url-panel');
  return {
    panel,
    utils,
  }
}

test('It should render result with origin and short field', () => {
  const mockData = [{origin: 'http://www.test.com', short: 'http://a.com/', _id: 'csc'}];
  const {utils} = setup(mockData);
  const text1Element = utils.getByText(/http:\/\/www.test.com/i);
  const text2Element = utils.getByText(/http:\/\/a.com/i);
  expect(text1Element).toBeInTheDocument();
  expect(text2Element).toBeInTheDocument();
});

test('It should render result for the corresponding length', () => {
  const mockData = [
    {origin: 'http://www.test.com', short: 'http://a.com/', _id: 'csc'},
    {origin: 'http://www.build.com', short: 'http://b.com/', _id: 'bac'},
    {origin: 'http://www.mask.com', short: 'http://c.com/', _id: 'wew'}
  ];
  const {panel} = setup(mockData);
  expect(panel).toHaveLength(mockData.length);
});

