import React, {FC} from 'react';
import type {UrlDataProps} from '../../type/type';
import {Wrapper, Group, Column, TheLink} from './UrlPanel.style';

export const UrlsPanel: FC<UrlDataProps> = ({urlData}) => {
  const renderUrls = !!urlData && urlData.map(data => (
    <Group key={data.short} aria-label="url-panel">
      <Column>{data.origin}</Column>
      <Column>
        <TheLink href={data.short} target="_blank">{data.short}</TheLink>
      </Column>
    </Group>
  ));
  return (
      <Wrapper>{renderUrls}</Wrapper>
  );
}