import React, {useState, FormEvent, FC} from 'react';

import {Wrapper, InputBox, Button, ErrorText} from './UrlInput.style';
import {checkValidUrl} from '../../helper/url.helper';
import {ICallbackProps} from '../../type/type';

export const UrlInput:FC<ICallbackProps> = ({callback}) => {
  const [url, setUrl] = useState<string>('');
  const [error, setError] = useState<boolean>(false);

  const onValueChange = (e: FormEvent<HTMLInputElement>):void => {
    if (error) {
      setError(false);
    }
    setUrl(e.currentTarget.value);
  };

  const submitUrl = ():void => {
    if (checkValidUrl(url)) {
      // submit link to api
      callback({origin: url});
      setUrl('');
    } else {
      setError(true)
    }
  }

  const renderErrorMessage = error && (
    <ErrorText>
      Please use an valid website url
    </ErrorText>
  );
  return (
    <>
      <Wrapper>
        <InputBox
          aria-label="url-input"
          placeholder="Please enter the url"
          onChange={onValueChange}
          value={url}
          hasError={error}
          autoComplete="off"
        />
        <Button onClick={submitUrl}>
          Shorten
        </Button>
      </Wrapper>
      {renderErrorMessage}
    </>
  );
}