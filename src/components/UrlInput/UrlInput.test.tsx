import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import {UrlInput} from './UrlInput';

const callback = jest.fn();
const setup = () => {
  const utils = render(<UrlInput callback={callback}/>);
  const input = utils.getByLabelText('url-input');
  const button = utils.getByRole('button', {name: /shorten/i});
  return {
    input,
    button,
    ...utils,
  }
}

test('It should update the input value', () => {
  const {input} = setup();
  fireEvent.change(input, {target: {value: 'http://test.com'}});
  expect(input.value).toBe('http://test.com')
});

test('It should trigger callback if value is a valid web url', () => {
  const {input, button} = setup();
  fireEvent.change(input, {target: {value: 'http://test.com'}});
  fireEvent.click(button);
  expect(callback).toHaveBeenCalledTimes(1);
});

test('It should not trigger callback if value is an invalid web url', () => {
  const {input, button} = setup();
  fireEvent.change(input, {target: {value: 'test test'}});
  fireEvent.click(button);
  expect(callback).toHaveBeenCalledTimes(0);
});
