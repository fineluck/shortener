import styled from "styled-components";

interface InputBoxProps {
  hasError: boolean;
}

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const InputBox = styled.input<InputBoxProps>`
  display: flex;
  box-sizing: border-box;
  width: 40rem;
  height: 4rem;
  padding: 0 1rem;
  border-width: 0.2rem;
  border-style: solid;
  border-color: ${p => p.hasError ? '#f44336' : '#333'};
  margin-right: 2rem;
  outline: none;
  font-size: 1.6rem;
  background-color: #fff;
  color: #212529;
`;

export const Button = styled.button`
  text-transform: uppercase;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  cursor: pointer;
  height: 4rem;
  width: 10rem;
  padding: 0 1rem;
  font-weight: bold;
  color: #00c3b9;
  border: 0.2rem solid #00c3b9;
  background-color: #fff;
  &:hover {
    color: #212529;
    background-color: #00c3b9;
  }
`;

export const ErrorText = styled.div`
  color: #f44336;
  font-size: 1.6rem;
  text-align: center;
  padding: 1rem;
  width: 100%100px;
`
