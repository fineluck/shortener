import styled from "styled-components";

export const Container = styled.div`
  background-color: #fff;
  padding: 20px;
  color: #212529;
`;

export const Header = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  min-height: 10rem;
`;

export const Logo = styled.img`
  height: 4rem;
  width: 18.4rem;
`;
