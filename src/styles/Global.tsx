import {createGlobalStyle} from 'styled-components';

 export const GlobalStyled = createGlobalStyle`
    html {
    box-sizing: border-box;
    font-size: 62.5%;
    }

    html,
    body {
    margin: 0;
    padding: 0;
    font-family: 'Open Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    }
 `;
