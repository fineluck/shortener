import React from 'react';
import {render, screen} from '@testing-library/react';
import App from './App';

test('renders shorten button', () => {
  render(<App />);
  const btnElement = screen.getByRole('button', {
    name: /shorten/i
  });
  expect(btnElement).toBeInTheDocument();
});

test('renders url input box', () => {
  render(<App />);
  const inputElement = screen.getByLabelText('url-input');
  expect(inputElement).toBeInTheDocument();
});
