import React, {useState, useEffect, useCallback, FC} from 'react';
import logo from './assets/logo.svg';
import {Container, Header} from './App.style'
import {UrlInput} from './components/UrlInput/UrlInput';
import {UrlsPanel} from './components/UrlsPanel/UrlsPanel';
import {GlobalStyled} from './styles/Global';
import {apiUrl} from './constant/api'

import type {UrlType, PostData} from './type/type';

const App:FC = () => {
  let [responseData, setResponseData] = useState<UrlType[] | []>([]);

  const fetchUrls = useCallback(() => {
    fetch(apiUrl)
    .then(res => res.json())
    .then((response) => {
      setResponseData(response)
    })
    .catch((error) => console.log(error));
  }, []);

  useEffect(() => fetchUrls(), [fetchUrls]);

  const submitUrl = (data: PostData):void => {
    fetch(apiUrl, {
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    })
    .then(res => res.json())
    .then((updatedUrl) => {
      const results = [...responseData, ...[updatedUrl]];
      setResponseData(results);
    })
    .catch((error) => console.log(error));
  };
  return (
    <Container className="App">
      <GlobalStyled />
      <Header>
        <img src={logo}  alt="logo" />
      </Header>
      <UrlInput callback={submitUrl} />
      <UrlsPanel urlData={responseData}/>
    </Container>
  );
}

export default App;
