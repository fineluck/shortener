export type UrlType = {
    origin: string;
    short: string;
    _id: string;
};

export type UrlDataProps = {
    urlData: UrlType[] | null;
};

export type PostData = {
    origin: string;
};

export interface ICallbackProps {
    callback: (data: PostData) => void;
 };
