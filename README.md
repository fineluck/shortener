# PrimaryBid URL Shortener

whole project is mainly using React, styled-component for the frontend, and nodejs, express, mongodb and mongoose for the backend.

## For Frontend

In the project directory, you can run:

### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

### `yarn build`

## For Backend

under `/server` folder, you can run

### `yarn start`

### `yarn test`

the express server will run on [http://localhost:7000](http://localhost:7000)

![Alt text](https://i.ibb.co/bKqbGN4/Screenshot-2021-10-01-at-14-37-59.png 'URL shortener')
