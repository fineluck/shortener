import {getShortUrl} from '../src/helpers/helper';

describe('auth', () => {
  it('should resolve with true and valid userId for hardcoded token', () => {
    const response = getShortUrl();
    expect(response).toContain('https://pbid.io/');
  });
})