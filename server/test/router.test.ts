/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { MongoClient } from 'mongodb';
const { MongoMemoryServer } = require('mongodb-memory-server');
const request = require('supertest');
const app = require('../src/index');


describe('Single MongoMemoryServer', () => {
  let con: MongoClient;
  let mongoServer: any;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    con = await MongoClient.connect(mongoServer.getUri(), );
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  const agent = request.agent(app);

  describe('GET /', () => {
    test('It should get origin url from db',  done => {
      agent
        .get('/')
        .expect(200)
        .then((res: any) => {
          expect(res.body).toBeTruthy();
          expect(res.body[0].origin).toBeTruthy();
          expect(res.body[0].short).toBeTruthy();
          done();
        });
    }, 30000);
  });

  describe('POST /', () => {
    test('It should save a shorten URL',  done => {
      const testData = {origin: 'http://www.test.com'};
      const targetDomain = 'https://pbid.io/';
      const rex = new RegExp(/^[a-zA-Z0-9_]{8}/);
      agent
        .post('/')
        .send(testData)
        .expect(200)
        .then((res: any) => {
          expect(res.body._id).toBeTruthy();
          expect(rex.test(res.body.short.split(targetDomain)[1])).toBeTruthy();
          expect(res.body.short).toContain(targetDomain)
          done();
        });
    }, 30000);
  });
});

