import mongoose, {Model, Document} from "mongoose";

export interface IUrl extends Document {
    origin: string
    short: string
}

const urlScheme = new mongoose.Schema({
    origin: {
        type: String,
        required: true
    },
    short: {
        type: String,
        required: true,
        unique: true
    }
})

export const UrlScheme: Model<Document> = mongoose.model('UrlScheme', urlScheme);