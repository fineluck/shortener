import dotenv from 'dotenv';
import express from 'express';
import cors from 'cors';
import mongoose from "mongoose";
import {json} from 'body-parser';

dotenv.config();

const PORT:number = parseInt(process.env.PORT as string, 10);

const app = express();
app.use(cors());
app.use(json());

const uri = 'mongodb+srv://cluster0.5rhtu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
mongoose.connect(uri, {
  user: process.env.DB_USER,
  pass: process.env.DB_PASSWORD
});
const con = mongoose.connection;
con.on('open', () => console.log('--- mongodb atlas connected'));

const theRoute = require('./routers/router');
app.use('/', theRoute);

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

module.exports = app;