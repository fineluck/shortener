export const getShortUrl = ():string => {
    const domain = 'https://pbid.io/';
    const id = require("crypto").randomBytes(20).toString('base64').substring(2,10);
    return domain + id;
}