import express, {Request, Response} from 'express';
import {getShortUrl} from '../helpers/helper';

import {UrlScheme} from '../model/urlModel';

const router = express.Router();

router.get('/', async(req: Request, res: Response) => {
    try {
        const results = UrlScheme.find({});
        results.exec((err, data) => {
            if (err) {
                res.send('Get Data Error' + err);
            }
            res.json(data);
        })
    } catch(e) {
        res.send('Error' + e);
    }
});

router.post('/', async(req: Request, res: Response) => {
    try {
        const origin = req.body.origin;
        let short;
        // or could move the logic to Schema.pre('save')
        const checkValue =  async():Promise<void> => {
            short = getShortUrl();
            if (await UrlScheme.exists({"short": short})) {
                checkValue();
            }
        }
        checkValue();
        const urls = new UrlScheme({
            origin,
            short
        });
        const results = await urls.save();
        res.json(results);
    } catch(e) {
        res.send('Error' + e)
    }
});

module.exports = router;